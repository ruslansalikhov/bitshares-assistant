[ENGLISH README](README.eng.md)

Телеграм бот, производящий уведомления о выполненных ордерах и изменениях цен.

Файл настроек setting.yaml:
```
    tg_token: <TELEGRAM BOT TOKEN>
    sentry_url: <SENTRY URL> # optional
    sentry_log_level: WARNING 

    redis_db_num: <REDIS DATABASE NUMBER>

    sentry_url: 

    log: /var/log/bitshares-assistant/bitshares-assistant.log
    log_level: INFO

    redis_db_num: 0
```

Бот не требует доступа к вашим ключам или иным приватным данным для работы, а напрямую анализирует публичную информацию с блокчейна по именами заданных аккаунтов.
В бесплатной версии бота доступен мониторинг одного аккаунта (при использовании @bitshares_assistant_bot). 


## Установка

### Ubuntu 16.04  
sudo apt install redis-server redis-client libffi-dev libssl-dev python-dev python3-pip 

pip3 install sqlalchemy bitshares telepot redis raven pyyaml

Или ```pip3 install requirements.txt```

В будущем будет нормальный инсталлятор, но пока так:

./install.sh
systemctl start notifier
systemctl start bot

Если сервисы работают:
systemctl enable notifier
systemctl enable bot


## Лицензия  

Данное программное обеспечение распространяется на условиях MIT лицензии.  

Автор: Dmitry Mantis. dmitry.mantis add_email_char_here protonmail com