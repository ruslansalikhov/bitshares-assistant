import yaml
from exceptions import *


def get_settings(setting_path):
    with open(setting_path) as f:
        setting = yaml.safe_load(f.read())
    if not 'tg_token' in setting:
        raise RequiredOptionMissedError('tg_token is required')
    return setting