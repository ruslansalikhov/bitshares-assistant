#!/usr/bin/env python3

import logging
import telepot
import telepot.aio
import asyncio
from telepot.aio.loop import MessageLoop
from raven.handlers.logging import SentryHandler
import os
from models import User, Account, Session
from utils import get_settings
from exceptions import *

os.chdir(os.path.dirname(os.path.abspath(__file__)))
setting = get_settings('setting.yaml')
logger = logging.getLogger('notifier.bot')
logger.setLevel(setting['log_level'])

bot = telepot.aio.Bot(setting['tg_token'])
session = Session()

if setting.get('sentry_url'):
    handler = SentryHandler(setting['sentry_url'])
    handler.setLevel(setting['sentry_log_level'])
    logger.addHandler(handler)

if setting.get('log'):
    fh = logging.FileHandler(setting['log'])
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(message)s')
    fh.setFormatter(formatter)
    logger.addHandler(fh)

welcome_msg = """
Welcome to BitShares Assistant!

To start monitor new account simply add it with /add command.
/add _bitshares account name_
/stop to stop getting updates.
/resume to resume getting updates.
/help to print this message again.
/status to print information about your settings.
/delete _bitshares account name_ to delete account from monitorin
/deleteall to delete all information about you from our database (all settings will be lost).
"""

info_msg = """
Your current accounts and monitor status: {accounts_list}

Notifications are *{status}*
"""

subscription_msg = """
To use more than one bitshares account subscription needed.

It costs only 50 BTS per month and will help to pay for servers
and maintance of the bot.

If you want to change the monitored account, just delete current data with /delete command.
"""

added_msg = "account *{}* has been successfuly added"
delete_account_msg = "*{}* has been removed"


def parse_user(msg):
    """ Parse 'from' field from message and return basic information
        about telegram user (id, username and name).

        :param msg: message to parse.
        :type msg: dict
    """
    from_ = msg.get('from')
    if not from_:
        raise EmptyFromFieldError()
    username = from_.get('username', '')
    first_name = from_.get('first_name', '')
    last_name = from_.get('last_name', '')
    if first_name or last_name:
        name = "{} {}".format(first_name, last_name)
    else:
        name = ""
    id_ = from_.get('id')
    if not id_:
        logger.error('invalid telegram id: {}'.format(id_))
        raise InvalidTelegramIDError()
    return id_, name, username


def parse_chat(msg):
    """ Parse 'chat' field from message and return chat id.

        :param msg: message to parse.
        :type msg: dict
    """
    chat = msg.get('chat')
    if not chat:
        raise EmptyChatFieldError()
    chat_id = chat.get('id')
    if not chat_id:
        raise InvalidTelegramIDError()
    return chat_id


def get_user(tg_user_id, tg_name='', tg_username=''):
    """ Return user model from database.
        
        Create user If user with such telegram id doesn't registered in
        database.
        
        :param tg_user_id: user's telegram id
        :type tg_user_id: integer
        :param tg_name: concatenation of user's telegram first_name and
        last_name
        :type tg_name: string
        :param tg_username: user's telegram username, if exist
        :type tg_username: string
    """
    user = session.query(User).filter_by(tg_id=tg_user_id).first()
    if not user:
        user = User(tg_user_id, tg_name, tg_username)
        session.add(user)
        session.commit()
    return user


def get_account(account_name):
    account = session.query(Account).filter_by(name=account_name).first()
    if not account:
        account = Account(account_name)
    return account


def cmd_add_handler(cmd, user):
    if cmd == '/add':
        return '/add command needs account name'
    try:
        account_name = cmd[5:].strip()
        user.add_account(get_account(account_name))
        resp = added_msg.format(account_name)
        logger.info('user {} added {} to monitoring'.format(
            user, account_name))
    except SubscriptionRequiredError:
        resp = subscription_msg
        session.rollback()
    except InvalidAccountNameError:
        resp = 'invalid account name'
        logging.warning("attempt to use invalid account name: '{}'".format(
            account_name))
        session.rollback()
    except AccountDoesNotExistsError:
        resp = "account *{}* doesn't exist".format(account_name)
        logging.warning(resp)
        session.rollback()
    return resp


def cmd_delete_all_handler(cmd, user):
    session.delete(user)
    logger.info('user {} has been deleted'.format(user))
    resp = 'your user information has been deleted'
    return resp


def cmd_delete_handler(cmd, user):
    if cmd == '/delete':
        return 'delete command needs account name'
    try:
        account_name = cmd[8:].strip()
        user.delete_account(get_account(account_name))
        logger.info('user {} removed {} from monitoring'.format(
            user, account_name))
        resp = delete_account_msg.format(account_name)
    except AccountNotLinkedError:
        resp = 'account *{}* is not in your monitor list'.format(account_name)
        session.rollback()
    except (InvalidAccountNameError, AccountDoesNotExistsError):
        resp = 'invalid account request'
        session.rollback()
    return resp


def cmd_resume_handler(cmd, user):
    user.resume_notifications()
    logger.info('user {} resumed notifications'.format(user))
    resp = 'nofications has been resumed'
    return resp


def cmd_stop_handler(cmd, user):
    user.stop_notifications()
    logger.info('user {} stopped notifications'.format(user))
    resp = 'nofications has been stopped'
    return resp


async def handle(msg):
    flavor = telepot.flavor(msg)
    if flavor != 'chat':
        logger.warning('not chat flavor is not expected: {}'.format(msg))
        return
    content_type, chat_type, chat_id = telepot.glance(msg, flavor=flavor)
    if content_type != 'text':
        logger.warning('not text content is not expected: {}'.format(msg))
        return

    logger.info('msg: {}'.format(msg))
    command = msg['text']

    try:
        chat_id = parse_chat(msg)
        user_id, name, username = parse_user(msg)
        user = get_user(user_id, name, username)
    except (InvalidTelegramIDError, EmptyFromFieldError, EmptyChatFieldError):
        logger.debug('skip handler due to exception')
        return

    resp = ''
    if command in ['/start', '/help']:
        resp = welcome_msg
    elif command.startswith('/add'):
        resp = cmd_add_handler(command, user)
    elif command == '/resume':
        resp = cmd_resume_handler(command, user)
    elif command == '/stop':
        resp = cmd_stop_handler(command, user)
    elif command == '/status':
        resp = info_msg.format(**user.get_info())
    elif command == '/deleteall':
        resp = cmd_delete_all_handler(command, user)
    elif command.startswith('/delete'):
        resp = cmd_delete_handler(command, user)
    elif command == '/orders':
        #  logger.info('user {} list orders command'.format(user))
        pass
    else:
        logger.info('invalid command: {}'.format(command))
        return
    session.commit()
    await bot.sendMessage(chat_id, resp, parse_mode='Markdown')


if __name__ == '__main__':
    logger.info('notifier bot service has been started')
    loop = asyncio.get_event_loop()
    loop.create_task(MessageLoop(bot, handle).run_forever())
    loop.run_forever()