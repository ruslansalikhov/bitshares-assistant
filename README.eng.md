[RUSSIAN README](README.ru.md)

Telegram Bot to monitor BitShares orders updates.
Bot doesn't need your keys or any other private information to function. It gets all neccessary public information from bitshares blockchain.

Free version includes monitor of one bitshares account. 

All donated BTS will go to rent better clouds, up more instances and improve bot quality.

Settings file setting.yaml:
```
    tg_token: <TELEGRAM BOT TOKEN>
    sentry_url: <SENTRY URL> # optional
    sentry_log_level: WARNING 

    redis_db_num: <REDIS DATABASE NUMBER>

    sentry_url: 

    log: /var/log/bitshares-assistant/bitshares-assistant.log
    log_level: INFO

    redis_db_num: 0
```

## Installation

### Ubuntu 16.04  
sudo apt install redis-server redis-client libffi-dev libssl-dev python-dev python3-pip

pip3 install sqlalchemy bitshares telepot redis raven pyyaml

or ```pip3 install requirements.txt```

In the future there would be better install script and debian packages as well

./install.sh
systemctl start notifier
systemctl start bot

Если сервисы работают:
systemctl enable notifier
systemctl enable bot

## License  

Software distributed under MIT license.

Author: Dmitry Mantis. dmitry.mantis add_email_char_here protonmail com
