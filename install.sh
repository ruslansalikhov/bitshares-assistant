#!/bin/bash

sudo apt install redis-server redis-client libffi-dev libssl-dev python-dev  
sudo adduser -q --disabled-password btsuser

sudo mkdir /opt/bitshares-assistant
sudo chown btsuser:btsuser /opt/bitshares-assistant

sudo mkdir /var/log/bitshares-assistant
sudo chown btsuser:btsuser /var/log/bitshares-assistant

sudo cp systemd/bot.service /etc/systemd/system/bot.service
sudo cp systemd/notifier.service /etc/systemd/system/notifier.service


