#!/usr/bin/env python3
"""
    OP_CODE = {
        'Transfer': 0,
        'NewOrder': 1,
        'CancelOrder': 2,
        'AdjustCollateral': 3,
        'FillOrder': 4,
        'Refferer': 5,
        'UpdateVotes': 6,
        'CreateAsset': 10,
        'Settle': 17,
        'ReportPrice': 19,
        'UpdateSingingKey': 21,
        'CreateProposal': 22,
        'UpdateProposal': 23
    }
"""

from bitshares.account import Account as BTSAccount
from bitshares.asset import Asset as BTSAsset
from models import Account, Session
from utils import get_settings
import logging
import telepot
import os
import time
import redis
from raven.handlers.logging import SentryHandler

from exceptions import *

os.chdir(os.path.dirname(os.path.abspath(__file__)))
setting = get_settings('setting.yaml')
logger = logging.getLogger('notifier')
logger.setLevel(setting['log_level'])

bot = telepot.Bot(setting['tg_token'])

redis = redis.Redis(db=setting['redis_db_num'])
session = Session()

if setting.get('sentry_url'):
    handler = SentryHandler(setting['sentry_url'])
    handler.setLevel(setting['sentry_log_level'])
    logger.addHandler(handler)

if setting.get('log'):
    fh = logging.FileHandler(setting['log'])
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(message)s')
    fh.setFormatter(formatter)
    logger.addHandler(fh)


OP_CODE = {
    'Transfer': 0,
    'NewOrder': 1,
    'CancelOrder': 2,
    'AdjustCollateral': 3,
    'FillOrder': 4,
    'Refferer': 5,
    'UpdateVotes': 6,
    'CreateAsset': 10,
    'UpdateAsset': 11,
    'Settle': 17,
    'ReportPrice': 19,
    'CreateProposal': 22,
    'UpdateProposal': 23
}


# todo: make infinite iterator instead of N last entries
def account_updates(account_name, stop_block):
    bts_account = BTSAccount(account_name)
    bts_history = bts_account.history(limit=100)

    first_entry = bts_history.__next__()
    first_block_num = first_entry['block_num']
    if stop_block is None:
        redis.set(account_name, first_block_num)
        stop_block = first_block_num
        logger.info('last_processed_block for account {} not found. set {} as '
                    'last_processed_block'.format(account_name, first_block_num))
    stop_block = int(stop_block)

    if first_entry['block_num'] <= stop_block:
        #  logger.debug('{}, {}'.format(first_entry['block_num'], stop_block))
        redis.set(account_name, first_block_num)
        raise StopIteration
    yield process_history_entry(first_entry)

    for history_entry in bts_history:
        if history_entry['block_num'] <= stop_block:
            redis.set(account_name, first_block_num)
            raise StopIteration
        yield process_history_entry(history_entry)
    redis.set(account_name, first_block_num)


class RequiredFieldMissedError(Exception):
    pass


# todo: add redis support
class Asset:
    assets_table = {}

    def __init__(self, asset_id, raw_amount=0):
        self.id = asset_id
        if not Asset.assets_table.get(asset_id):
            logger.info('asset {} not found in assets_table, download.'.format(
                asset_id))
            asset = BTSAsset(asset_id)
            Asset.assets_table[asset_id] = {}
            # more checks?
            Asset.assets_table[asset_id]['symbol'] = asset['symbol']
            Asset.assets_table[asset_id]['precision'] = int(asset['precision'])
        self.symbol = Asset.assets_table[asset_id]['symbol']
        self.precision = Asset.assets_table[asset_id]['precision']
        self.raw_amount = int(raw_amount)

    # todo
    def load_assets_table_from_redis():
        pass

    # todo: REWRITE WITH SAFE DECIMAL HANDLE
    @property
    def amount(self):
        return self.raw_amount / (10 ** self.precision)


def check_required_fields(required_fields, dict_to_check, err_msg):
    for required_field in required_fields:
        if required_field not in dict_to_check:
            logger.error(err_msg.format(required_field, dict_to_check))
            raise RequiredFieldMissedError()


def fill_order_handler(op_info):
    """ Process FillOrder operation. """
    if 'receives' not in op_info:
        logger.error("missed required field 'receives' in fill order op_info")
    if 'pays' not in op_info:
        logger.error("missed required field 'pays' in fill order op_info")
    receives, pays = op_info['receives'], op_info['pays']

    asset1 = Asset(receives['asset_id'], receives['amount'])
    asset2 = Asset(pays['asset_id'], pays['amount'])
    return "Bought {0} {1} for {2} {3}".format(
        asset1.amount, asset1.symbol, asset2.amount, asset2.symbol)


def transfer_handler(op_info):
    err_msg = "missed required field '{0}' in transfer op_info: {1}"
    required_fields = ['from', 'to', 'amount']
    check_required_fields(required_fields, op_info, err_msg)
    account_from = BTSAccount(op_info['from']).name
    account_to = BTSAccount(op_info['to']).name
    asset = Asset(op_info['amount']['asset_id'], op_info['amount']['amount'])
    return "Transfer {0} {1} from *{2}* to *{3}*".format(
        asset.amount, asset.symbol, account_from, account_to)


def new_order_handler(op_info):
    err_msg = "missed required field '{0}' in new_order_handler op_info: {1}"
    required_fields = ['amount_to_sell', 'min_to_receive']
    check_required_fields(required_fields, op_info, err_msg)

    amount_to_sell = op_info['amount_to_sell']
    min_to_receive = op_info['min_to_receive']
    asset1 = Asset(amount_to_sell['asset_id'], amount_to_sell['amount'])
    asset2 = Asset(min_to_receive['asset_id'], min_to_receive['amount'])
    new_order_msg = ("Wants to sell {asset1.amount} {asset1.symbol} for "
                     "{asset2.amount} {asset2.symbol}")
    return new_order_msg.format(asset1=asset1, asset2=asset2)


def cancel_order_handler(op_result):
    if not op_result or len(op_result) < 2:
        logger.error('no result field for cancel order') # bad!
        raise RequiredFieldMissedError()
    asset_id = op_result[1].get('asset_id')
    amount = op_result[1].get('amount')
    """
    if asset_id is None:
        logger.error("missed field 'asset_id' in cancel order")
        raise RequiredFieldMissedError()
    if amount is None:
        logger.error("missed field 'amount' in cancel order {}".format(op_result))
        raise RequiredFieldMissedError()
    """
    if asset_id is None or amount is None:
        return None
    asset = Asset(asset_id, amount)
    return "Cancel order on {} {}".format(
        asset.amount, asset.symbol)


def referrer_handler(op_info):
    err_msg = "missed required field '{0}' in referrer_handler op_info: {1}"
    required_fields = ['name', 'referrer_percent', 'registrar', 'referrer']
    check_required_fields(required_fields, op_info, err_msg)
    registrar = BTSAccount(op_info['registrar']).name
    referrer = BTSAccount(op_info['referrer']).name
    new_account = op_info['name']
    return "*{0}* register new account *{1}* thanks to *{2}*".format(
        registrar, new_account, referrer)


def fill_settle_handler(op_info):
    return None


def settle_handler(op_info):
    if 'amount' not in op_info:
        raise RequiredFieldMissedError()
    asset = Asset(op_info['amount']['asset_id'], op_info['amount']['amount'])
    return "settle {0} {1}".format(asset.amount, asset.symbol)


def process_history_entry(history_entry):
    """ Process account's history entry.

        Following operations are handled: create order, cancel order, transfer,
        referrer.
        
        Following operations are ignored: assets creation, asset update,
        price reports, update singing key, create proposal, update proposal.
    """
    logger.debug('process history entry {}'.format(history_entry))
    op_handlers = {
        0: transfer_handler,
        1: new_order_handler,
        2: cancel_order_handler,
        3: fill_settle_handler,
        4: fill_order_handler,
        5: referrer_handler,
        17: settle_handler
    }

    operation = history_entry.get('op')
    if not operation:
        logger.warning('no operations found: {}'.format(history_entry))
    op_code, op_info = operation[0], operation[1]
    try:
        if op_code in [0, 1, 3, 4, 5, 17]:
            return op_handlers[op_code](op_info)
        elif op_code == 2:
            op_result = history_entry.get('result', [])
            return op_handlers[op_code](op_result)
        elif op_code in [6, 10, 11, 19, 21, 22, 23]:
            return None
        else:
            raise UnknownOperationCodeError()
    except RequiredFieldMissedError:
        logger.error('required field missed in {}'.format(history_entry))
        return None
    except UnknownOperationCodeError:
        logger.error('unknown op_code: {} in {}'.format(
                     op_code, history_entry))
        return None


def broadcast_message(users, msg):
    for user in users:
        if user.status:
            try:
                bot.sendMessage(user.tg_id, msg, parse_mode='Markdown')
                logger.debug('send {} to {}'.format(msg, user.tg_id))
            except telepot.exception.BotWasBlockedError:
                logger.warning('user {} blocked the bot. Disable notifications.'.format(user.tg_id))
                user.status = 0
                session.commit()
            # dirty hack to avoid telegram limits. We can't send more frequently anyway.
            time.sleep(0.0333)


def process_account(account):
    account_name = account.name
    logger.debug('process account {}'.format(account_name))
    stop_block = redis.get(account_name)
    update_msgs = []
    for update_msg in account_updates(account_name, stop_block):
        if update_msg:
            update_msgs.append(update_msg)
    if update_msgs:
        msg = "New updates on account *{}*:\n\n • ".format(account_name)
        msg = msg + "\n • ".join(update_msgs)
        broadcast_message(account.users, msg)


def process_loop(check_interval=15):
    while True:
        lookup_accounts = session.query(Account).all()
        logger.info('lookup_accounts: {}'.format(lookup_accounts))
        for lookup_account in lookup_accounts:
            process_account(lookup_account)
        session.expire_all()
        time.sleep(check_interval)


if __name__ == '__main__':
    logger.info('notifier service has been started')
    check_interval = setting.get('check_interval', 15)
    process_loop(check_interval)
