from sqlalchemy import Integer, String, Boolean, DateTime, Column, Table
from sqlalchemy import create_engine, PrimaryKeyConstraint, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker
from bitshares.exceptions import AccountDoesNotExistsException
from bitshares import account
import string
import os

from exceptions import *

# todo move to posgresql

os.chdir(os.path.dirname(os.path.abspath(__file__)))
# setting = get_settings('setting.yaml')
engine = create_engine('sqlite:///test.db')
Session = sessionmaker(bind=engine)
Base = declarative_base()

association_table = Table('association', Base.metadata,
    Column('user_id', Integer, ForeignKey('users.id'), primary_key=True),
    Column('account_id', Integer, ForeignKey('accounts.id'), primary_key=True),
    PrimaryKeyConstraint('user_id', 'account_id')
)


class User(Base):
    """ Telegram User. Many-To-Many relationship with Account. """
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    tg_id = Column(Integer, unique=True)
    tg_name = Column(String, default='')
    tg_username = Column(String, default='')
    pro = Column(Boolean, default=False)
    pro_dt = Column(DateTime)
    status = Column(Boolean, default=True)
    accounts = relationship("Account", secondary=association_table,
                            back_populates="users")

    def __init__(self, tg_id, tg_name='', tg_username=''):
        self.tg_id = tg_id
        self.tg_name = tg_name
        self.tg_username = tg_username

    def __repr__(self):
        pro = 'pro' if self.pro else 'regular'
        return "<User({},'{}',{} account)>".format(
            self.tg_id, self.tg_name, pro)

    def add_account(self, account):
        if len(self.accounts) >= 1 and self.pro == 0:
            raise SubscriptionRequiredError()
        self.accounts.append(account)

    def resume_notifications(self):
        self.status = True

    def stop_notifications(self):
        self.status = False

    def get_info(self):
        accounts_list = ('\n • {}'.format(a.name) for a in self.accounts)
        info_dict = {
            'accounts_list': ''.join(accounts_list),
            'status': 'on' if self.status else 'off'
        }
        return info_dict

    def delete_account(self, account):
        try:
            self.accounts.remove(account)
        except ValueError:
            raise AccountNotLinkedError()


class Account(Base):
    """ BitShares Account. Many-To-Many relationship with User. """
    __tablename__ = 'accounts'
    id = Column(Integer, primary_key=True)
    bts_id = Column(String, unique=True)
    name = Column(String, unique=True, index=True)
    users = relationship("User", secondary=association_table,
                         back_populates="accounts")

    def __init__(self, account_name):
        allowed_chars = string.ascii_lowercase + string.digits + '-.'
        if any(c not in allowed_chars for c in account_name):
            raise InvalidAccountNameError()
        try:
            self.bts_id = account.Account(account_name).get('id')
        except AccountDoesNotExistsException:
            raise AccountDoesNotExistsError()
        self.name = account_name

    def __repr__(self):
        return "<Account('{}','{}')>".format(self.name, self.bts_id)


def init_db():
    Base.metadata.create_all(engine)


def dumb_data():
    u1 = User(123)
    a1 = Account('test-acc')
    u1.accounts.append(a1)
    session.add(u1)
    session.commit()


if __name__ == '__main__':
    session = Session()
    init_db()
    # dumb_data()
